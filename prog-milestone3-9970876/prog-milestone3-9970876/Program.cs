﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9970876
{
    public static class Details
    {
        public static string name;
        public static string address;

        public static void customerDetails()
        {
            int x;
            var dispLine = new string('*', 60);

            Console.Clear();

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("Please enter your name: ");
            Console.WriteLine();
            Console.WriteLine(dispLine);

            name = Console.ReadLine();

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("Now enter an address for the delivery of your order: ");
            Console.WriteLine();
            Console.WriteLine(dispLine);

            address = Console.ReadLine();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var dispLine = new string('*', 60);
            var type = new List<string> { "Meatlovers", "Seafood", "Vege", "Hawaiian", "Chicken", "Supreme Cheese" };
            var orders = new List<Tuple<string, string, double>>();
            string typeofPizza;
            string owed;
            int i;
            int x;
            double price = 0;
            double paid = 0;

            List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
            pizza.Add(Tuple.Create("Small", 9.00));
            pizza.Add(Tuple.Create("Medium", 14.00));
            pizza.Add(Tuple.Create("Large", 20.00));
            pizza.Add(Tuple.Create("Mega", 25.00));

            List<Tuple<string, double>> extras = new List<Tuple<string, double>>();
            extras.Add(Tuple.Create("Stuffed Crust", 4.00));
            extras.Add(Tuple.Create("More Cheese", 6.00));
            extras.Add(Tuple.Create("Double Everything", 10.00));
            extras.Add(Tuple.Create("More Sauce", 2.00));

            List<Tuple<string, double>> drink = new List<Tuple<string, double>>();
            drink.Add(Tuple.Create("Coke", 3.00));
            drink.Add(Tuple.Create("Sprite", 3.00));
            drink.Add(Tuple.Create("Fanta", 3.00));
            drink.Add(Tuple.Create("L&P", 3.00));
            drink.Add(Tuple.Create("Orange Juice", 4.00));
            drink.Add(Tuple.Create("Lift", 3.00));

            List<Tuple<string, double>> sides = new List<Tuple<string, double>>();
            sides.Add(Tuple.Create("Nuggets", 5.00));
            sides.Add(Tuple.Create("Chips", 4.00));
            sides.Add(Tuple.Create("Wings", 8.00));
            sides.Add(Tuple.Create("Garlic Bread", 4.00));
            sides.Add(Tuple.Create("Icecream", 3.00));

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("Hello and welcome to some place that sells pizza, we are happy to take your order");
            Console.WriteLine();
            Console.WriteLine("Please press any key to continue and order: ");
            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.ReadKey();
            Details.customerDetails();



        menu:

            Console.Clear();
            Display.displayDetails();
            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine("Menu:");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Extras");
            Console.WriteLine("3. Drinks");
            Console.WriteLine("4. Sides");
            Console.WriteLine("5. Confirm Order");
            Console.WriteLine();
            Console.WriteLine(dispLine);


            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:

                    Pizza:

                        Console.Clear();
                        Display.displayDetails();

                        for (i = 0; i < type.Count; i++)
                        {
                            Console.WriteLine();
                            Console.WriteLine($"{i + 1}. {type[i]}");
                            Console.WriteLine();                            
                        }

                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}, Menu.");
                        Console.WriteLine();
                        Console.WriteLine(dispLine);

                        if (int.TryParse(Console.ReadLine(), out x))
                        {

                            if (x == i + 1)
                            {
                                goto menu;
                            }

                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                Console.WriteLine();
                                Console.ReadLine();

                                goto Pizza;
                            }

                            else
                            {
                                typeofPizza = type[x - 1];

                                Console.Clear();
                                Display.displayDetails();
                                Console.WriteLine(dispLine);
                                Console.WriteLine();

                                for (i = 0; i < pizza.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {pizza[i].Item1}");
                                }

                                Console.WriteLine();
                                Console.WriteLine($"{i + 1}, Menu");
                                Console.WriteLine();
                                Console.WriteLine(dispLine);

                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto menu;
                                    }

                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine();
                                        Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                        Console.WriteLine();
                                        Console.ReadLine();

                                        goto Pizza;
                                    }

                                    else
                                    {
                                        orders.Add(Tuple.Create(typeofPizza, pizza[x - 1].Item1, pizza[x - 1].Item2));

                                        Display.orders.Add(Tuple.Create(typeofPizza, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                    }

                                    goto Pizza;
                                }

                                else
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                    Console.WriteLine();
                                    Console.ReadLine();

                                    goto Pizza;
                                }
                            }

                        }

                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("You have entered an incorrect prompt, please try again.");
                            Console.WriteLine();
                            Console.ReadLine();

                            goto Pizza;
                        }

                    case 2:

                    Extras:

                        Console.Clear();
                        Display.displayDetails();
                        Console.WriteLine(dispLine);
                        Console.WriteLine();

                        for (i = 0; i < extras.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {extras[i].Item1}");
                        }

                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}, Menu");
                        Console.WriteLine();
                        Console.WriteLine(dispLine);

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                Console.WriteLine();
                                Console.ReadLine();

                                goto Extras;
                            }
                            else
                            {
                                orders.Add(Tuple.Create("Extras", extras[x - 1].Item1, extras[x - 1].Item2));

                                Display.orders.Add(Tuple.Create("Extras", extras[x - 1].Item1, extras[x - 1].Item2));
                            }

                            goto Extras;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("You have entered an incorrect prompt, please try again.");
                            Console.WriteLine();
                            Console.ReadLine();

                            goto Extras;
                        }

                    case 3:
                    Drinks:

                        Console.Clear();
                        Display.displayDetails();
                        Console.WriteLine(dispLine);
                        Console.WriteLine();

                        for (i = 0; i < drink.Count; i++)

                        {
                            Console.WriteLine($"{i + 1}. {drink[i].Item1}");
                        }

                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}, Menu");
                        Console.WriteLine();
                        Console.WriteLine(dispLine);

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                Console.WriteLine();
                                Console.ReadLine();

                                goto Drinks;
                            }
                            else
                            {
                                orders.Add(Tuple.Create("Drink", drink[x - 1].Item1, drink[x - 1].Item2));

                                Display.orders.Add(Tuple.Create("Drink", drink[x - 1].Item1, drink[x - 1].Item2));
                            }

                            goto Drinks;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("You have entered an incorrect prompt, please try again.");
                            Console.WriteLine();
                            Console.ReadLine();

                            goto Drinks;
                        }

                    case 4:

                    Sides:

                        Console.Clear();
                        Display.displayDetails();
                        Console.WriteLine(dispLine);
                        Console.WriteLine();

                        for (i = 0; i < sides.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {sides[i].Item1}");
                        }

                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}, Menu");
                        Console.WriteLine();
                        Console.WriteLine(dispLine);

                        if (int.TryParse(Console.ReadLine(), out x))

                        {
                            if (x == i + 1)
                            {
                                goto menu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("You have entered an incorrect prompt, please try again.");
                                Console.WriteLine();
                                Console.ReadLine();

                                goto Sides;
                            }
                            else
                            {
                                orders.Add(Tuple.Create("Sides", sides[x - 1].Item1, sides[x - 1].Item2));

                                Display.orders.Add(Tuple.Create("Sides", sides[x - 1].Item1, sides[x - 1].Item2));
                            }

                            goto Sides;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("You have entered an incorrect prompt, please try again.");
                            Console.WriteLine();
                            Console.ReadLine();

                            goto Sides;
                        }

                    case 5:

                    Confirmation:


                        Console.Clear();
                        Display.displayDetails();
                        Console.WriteLine();
                        Console.WriteLine("Is this correct?");
                        Console.WriteLine("Yes: (1)");
                        Console.WriteLine("No: (2)");
                        Console.WriteLine();
                        Console.WriteLine(dispLine);

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            switch (x)
                            {
                                case 1:

                                    for (i = 0; i < orders.Count; i++)
                                    {
                                        owed = String.Format("{0:C}", orders[i].Item3);

                                        Console.Clear();
                                        Console.WriteLine($"{orders[i].Item1} {orders[i].Item2} {owed}");

                                        price = price + orders[i].Item3;
                                    }

                                    owed = String.Format("{0:C}", price);

                                    Console.Clear();
                                    Console.WriteLine(dispLine);
                                    Console.WriteLine();
                                    Console.WriteLine($"Price: ${price}");
                                    Console.WriteLine();
                                    Console.WriteLine("Please make your payment now: ");
                                    Console.WriteLine();
                                    Console.WriteLine(dispLine);

                                    paid = double.Parse(Console.ReadLine());

                                    if (paid > price)
                                    {
                                        paid = paid - price;

                                        Console.Clear();
                                        Console.WriteLine(dispLine);
                                        Console.WriteLine();
                                        Console.WriteLine($"Thankyou, your change is: {paid}");
                                        Console.WriteLine("Press any key to exit.");
                                        Console.WriteLine();
                                        Console.WriteLine(dispLine);
                                        Console.ReadLine();
                                    }

                                    else if (paid == price)
                                    {
                                        Console.Clear();
                                        Console.WriteLine(dispLine);
                                        Console.WriteLine();
                                        Console.WriteLine("Thankyou for your purchase.");
                                        Console.WriteLine("Press any key to exit.");
                                        Console.WriteLine();
                                        Console.WriteLine(dispLine);
                                        Console.ReadLine();
                                    }

                                    else if (paid < price)
                                    {
                                        Console.WriteLine(dispLine);
                                        Console.WriteLine();
                                        Console.WriteLine("Sorry, you don't have enough money to make this purchase, please try again.");
                                        Console.WriteLine();
                                        Console.WriteLine(dispLine);
                                        Console.ReadLine();

                                        goto Confirmation;
                                    }

                                    Environment.Exit(0);

                                    break;

                                case 2:

                                    goto menu;
                            }
                        }

                        break;
                }
            }

            else
            {
                Console.WriteLine(dispLine);
                Console.WriteLine();
                Console.WriteLine("You entered an incorrect prompt, please try again.");
                Console.WriteLine();
                Console.WriteLine(dispLine);
                Console.ReadLine();

                goto menu;
            }

            Display.displayDetails();

        }
    }

    public static class Display
    {
        public static List<Tuple<string, string, double>> orders = new List<Tuple<string, string, double>>();

        public static void displayDetails()
        {
            int i;
            double cost = 0;
            string owed;
            var dispLine = new string('*', 60);

            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"Name: {Details.name}");
            Console.WriteLine($"Address: {Details.address}");

            Console.WriteLine("Order Summary:");

            for (i = 0; i < orders.Count; i++)
            {
                owed = String.Format("{0:C}", orders[i].Item3);

                Console.WriteLine($"{orders[i].Item1}, {orders[i].Item2}, {owed}");

                cost = cost + orders[i].Item3;
            }
            owed = String.Format("{0:C}", cost);
            
            Console.WriteLine();
            Console.WriteLine($"cost: ${cost}");
            Console.WriteLine();
            Console.WriteLine(dispLine);

        }
    }
}
